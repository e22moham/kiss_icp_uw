# Questions
<ol>
<li>
How would you go on fusing the data from different sensors with different frequencies?
<br>
<br>
There are several important factors to consider when fusing data from different frequencies. Firstly, it is always desirable to synchronize the sensors using a pulse-per-second signal, for instance, to ensure hardware synchronization. If the signals from different sensors are not synchronous in time, time interpolation can be used for low-dimensional signals (e.g., IMU) to synchronize them with the other signals. However, interpolation is not feasible for high-dimensional signals, such as a camera's image stream or Lidar's point clouds. Asynchronous sensor fusion techniques, such as the asynchronous Kalman filter, are needed in such cases to take varying time steps into account in the fusion equations.
<br>
<br>
If the signals are synchronized but coming in different frequencies, each signal can be used in its original frequency or sub-sampled to a lower frequency, if necessary, to reduce computational load. Several algorithms can be used to fuse data with different frequencies. Kalman filters, for example, can handle this by including multiple individual update steps based on the arrival of each measurement signal at different frequencies. Optimization algorithms based on Maximum Likelihood in the form of a least-squared problem can also handle this by forming a residual term for each sensor measurement that doesn't have to be in a single frequency.
<br>
<br>
<li> What role do the covariances play when fusing the data?
<br>
<br>
Covariance matrices generally define the variability of a vector of random variables. In sensor fusion, they describe the variability of the noise associated with a measurement or the error in a process/measurement model. In a Kalman filter, for example, the covariance matrix of a measurement determines how much the fusion relies on that measurement. In a least-squares fusion scheme, covariance matrices serve as the inverse of weights that are multiplied by residual terms. Therefore, a larger covariance matrix indicates higher uncertainty associated with the corresponding residual, and the fusion output is less affected/determined by the residual term.
<br>
<br>
<li>What issues have you faced with the RTK-GPS system? How did you handle them?
<br>
<br>
While working on the WATonoBus project at the University of Waterloo, I encountered several challenges when working with an RTK-GNSS system. First and foremost, establishing a reliable connection to the base station was essential. We utilized a 4G cellular connection, but adverse weather conditions sometimes resulted in a loss of cellular connection and no access to the base station, in addition to the atmospheric effects on the GNSS measurement itself. Another issue we faced was the high unit cost of an RTK-GNSS system, making it unscalable.
<br>
<br>
<li>
If one of the sensors has gone faulty during the run-time, how will you handle it?
<br>
<br>
The approach to handling sensor failures depends on the type of failure. Hardware failures can be monitored by tracking the health of the received signal, while software faults require redundancy in the sensor data. For example, if GNSS fails, an analysis of the predictions from an accurate Lidar odometry system can detect the fault. Alternatively, comparing the speed provided by GNSS-INS with that of the wheel encoders can reveal GNSS faults. Time-based analysis is another way to detect sensor faults, as a sudden change in a sensor's behavior may indicate a potential fault.
<br>
<br>
To manage faults, a health monitoring system should publish a warning message that specifies the type of fault. Decision-making and safety modules can then react accordingly by safely bringing the robot to a full stop if necessary.
<br>
<br>
<li>
How will you check if the re-localization in a pre-built map was successful or not? Please answer this question for two systems: one with GPS and one without. 
<br>
<br>
Redundancy in the sensor suit is one approach to ensure successful relocalization. When GPS is available, we can compare the relocalization results with GPS. If the difference between the relocalization and GPS output exceeds a threshold (e.g., three times the standard deviation of the GPS measurement at the moment), we can conclude that the relocalization results are not successful.
<br>
<br>
In the absence of GPS, other criteria can be used to evaluate the success of relocalization. If the robot's recent position is available, we can approximate how much it should have traveled during the localization drop-out based on simple motion models (e.g., constant velocity model). We can then compare the relocalization results with the predicted location of the robot.
<br>
<br>
Furthermore, the relocalization algorithm itself may provide additional information about its output. If the relocalization is performed using an iterative optimization problem, reaching the maximum iteration limit means that the optimal solution has not been found, and the relocalization output may be false.
<br>
<br>
<li>How can you get the AHRS from an IMU? What are the common issues which you have come across?
<br>
<br>
To obtain AHRS from an IMU (which includes a gyroscope, accelerometer, and magnetometer), an Extended Kalman Filter (EKF) can be used to estimate the orientation of the sensor. However, a common issue with this system is the presence of time-varying biases, which can cause the calibration to change over time. Additionally, sustained dynamic acceleration or deceleration maneuvers, as well as magnetic disturbances added to the magnetometer, can pose challenges for AHRS.
<br>
<br>
<li>Why are IMUs used with Lidars and Cameras? What are the common issues?
<br>
<br>
Using an IMU with a camera and a Lidar can improve SLAM performance by imposing additional constraints on the robot's motion. This is particularly useful in challenging environments, such as dynamic or low-light/low-textured environments, where camera or Lidar measurements may become unreliable.
<br>
<br>
One common issue when fusing an IMU with a camera/Lidar is accurately estimating IMU biases. This is often done through tightly fusing IMU biases by including them in the state vector. However, estimating IMU biases can be challenging for ground vehicles due to their degenerative planar motions, which can make the problem unobservable.
<br>
<br>
Another practical challenge when fusing an IMU with a camera/Lidar is obtaining an accurate extrinsic calibration of the IMU to the camera/Lidar frames. This can be achieved by including the extrinsic calibration parameters in the estimation problem. However, a good initial guess of the calibration is required, which can be obtained by manually measuring the lever arms between the sensors.
<br>
<br>
In addition, fusing an IMU with a camera/Lidar can also present challenges related to handling different frequencies. IMU measurements are typically provided at higher frequencies (e.g. 100 Hz) compared to cameras/Lidars (e.g. 10 Hz). In Visual Inertial Navigation Systems (VINS), the industry standard approach to handle this issue is to use pre-integrated factors of the IMU in the optimization problem by transforming the IMU measurement into the robot body frame. This significantly reduces the computational complexity of the problem.
<br>
<br>
<li>How will you go on selecting a perception sensor for SLAM? (Kindly mention the common criterias)
<br>
<br>
The following are the criteria for selecting the sensor modalities for SLAM:
<ul>

<li>indoor/outdoor/underwater
<li>Range
<li>Accuracy
<li>weather conditions
<li>cost
<li>weight
<li>power consumption
</ul>
</ol>
