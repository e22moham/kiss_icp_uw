# kiss_icp_uw
This repository provides a very easy way of running [kiss_icp](https://github.com/PRBonn/kiss-icp) SLAM algorithm using the docker image [mbagher/kiss_icp](https://hub.docker.com/r/mbagher/kiss_icp) that I created.

I implemented [kiss_icp](https://github.com/PRBonn/kiss-icp) because it is state-of-the-art Lidar SLAM algorithm that works in real-time (it takes about 2-3 cores of 3.60GHz CPU). The algorithm has on-par accuracy with other state-of-the-art Lidar SLAM algorithms without fusing with IMU or GPS.

# Requirments
The only requirement is to have Ubuntu with [docker](https://docs.docker.com/engine/install/ubuntu/) installed. The docker container takes care of all other required dependencies.

# Instructions
<ol>
<li> Place <strong>rad_test_1.bag</strong> file into the directory: <strong>home/$USER/Downloads</strong>

<li>Clone the current git repository on your machine:

<code>git clone https://git.uwaterloo.ca/e22moham/kiss_icp_uw.git</code>

<li> Change the current directory:

<code>cd kiss_icp_uw</code>

<li>Run the following bash scripts:

<code>sh run_kiss.sh</code>
</ol>

# My answers to the Interview Questions
Please review the [questions.md](questions.md) file that contains the answers to the interview questions.

## Discussion on the potential challenges for deploying on a production vehicle
The following future work are necessary before the algorithm becomes ready to be deployed on a production vehicle:
<ul>
<li>Adding the localization-only mode that register Lidar point cloud with static point cloud map to avoid doing mapping all the time.
<li>Adding a relocalization thread to solve initialization and kidnapped-robot problems.
<li>Adding loop-closure thread to limit localization drift during SLAM mode.
<li>Adding a map-maintenance module that frequently updates the static map due to the changes in the environment (construction, seasonal changes, etc.)
<li>Collect, analyze, and report the localization performance in a variaty of operational conditions to ensure reliablity of the localization output.
<li>Fusion with other sensor modalities such as IMU to enable the system work in adverse weather conditions (e.g. rain, fog, snow)
</ul>
