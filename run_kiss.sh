
docker_img='mbagher/kiss_icp'
container_name='kiss_icp_cont'

if [ "$( docker container inspect -f '{{.State.Running}}' $container_name 2> /dev/null)" = "true" ]
then
	docker exec -it $container_name bash
else	
	gnome-terminal --tab -- roscore
	sleep 1
	gnome-terminal --tab -- bash -c "rosrun rviz rviz -d ./rviz/kiss_icp_ros1.rviz; exec bash"
	sleep 1
	docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -v /home/$USER/Downloads:/root/Downloads -e DISPLAY --name=$container_name --net=host --rm --gpus all  $docker_img bash -c "/root/run.sh"
fi 
